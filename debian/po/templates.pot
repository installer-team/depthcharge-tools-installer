# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the depthcharge-tools-installer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: depthcharge-tools-installer\n"
"Report-Msgid-Bugs-To: depthcharge-tools-installer@packages.debian.org\n"
"POT-Creation-Date: 2023-06-26 20:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#. Main menu entry.
#. :sl4:
#: ../templates:1001
msgid "Make this ChromeOS board bootable"
msgstr ""

#. Type: note
#. Description
#. Progress bar title.
#. :sl4:
#: ../templates:2001
msgid "Making this ChromeOS board bootable"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:3001
msgid "Installing tools to manage ChromeOS boot images"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:4001
msgid "Checking ChromeOS kernel partitions"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:5001
msgid "Updating initramfs"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:6001
msgid "Building a boot image"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:7001
msgid "Writing the boot image to disk"
msgstr ""

#. Type: error
#. Description
#. Same as base-installer/kernel/failed-package-install.
#. :sl4:
#: ../templates:8001
msgid "Unable to install ${PACKAGE}"
msgstr ""

#. Type: error
#. Description
#. Same as base-installer/kernel/failed-package-install.
#. :sl4:
#: ../templates:8001
msgid ""
"An error was returned while trying to install the ${PACKAGE} package onto "
"the target system."
msgstr ""

#. Type: error
#. Description
#. Same as base-installer/kernel/failed-package-install.
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../templates:8001 ../templates:9001 ../templates:10001 ../templates:11001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:9001
msgid "No usable ChromeOS kernel partition is found"
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:9001
msgid "An error was returned while searching for a ChromeOS kernel partition."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:10001
msgid "Cannot build a boot image"
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:10001
msgid "An error was returned while building a boot image."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:11001
msgid "Cannot write boot image to disk"
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:11001
msgid "An error was returned while writing the boot image to disk."
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../templates:12001
msgid "Reconfigure initramfs policies?"
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../templates:12001
msgid ""
"Could not generate a small enough boot image for this board. Usually this "
"can be resolved by including less modules in the initramfs."
msgstr ""
