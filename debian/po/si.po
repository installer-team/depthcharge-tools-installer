# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
#
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
#
#
# Danishka Navin <danishka@gmail.com>, 2009, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: depthcharge-tools-installer@packages.debian.org\n"
"POT-Creation-Date: 2023-06-26 20:02+0000\n"
"PO-Revision-Date: 2023-03-01 15:39+0000\n"
"Last-Translator: HelaBasa Group <weblate-helabasa@yalu.lk>\n"
"Language-Team: Sinhala <info@hanthana.org>\n"
"Language: si\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: note
#. Description
#. Main menu entry.
#. :sl4:
#: ../templates:1001
#, fuzzy
msgid "Make this ChromeOS board bootable"
msgstr "පද්ධතිය ඇරඹිය-හැකි තත්වයට හරවන්න"

#. Type: note
#. Description
#. Progress bar title.
#. :sl4:
#: ../templates:2001
#, fuzzy
msgid "Making this ChromeOS board bootable"
msgstr "පද්ධතිය ආරම්භකලහැකි ලෙස සකසමින්"

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:3001
#, fuzzy
msgid "Installing tools to manage ChromeOS boot images"
msgstr "Cobalt ස්ථාපකය ස්ථාපනය අසමත්විය."

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:4001
#, fuzzy
msgid "Checking ChromeOS kernel partitions"
msgstr "කොටස් පිරික්සමින්"

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:5001
msgid "Updating initramfs"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:6001
msgid "Building a boot image"
msgstr ""

#. Type: note
#. Description
#. Progress bar step.
#. :sl4:
#: ../templates:7001
#, fuzzy
msgid "Writing the boot image to disk"
msgstr "ආරම්භක පිළිබිඹුව තැටිය මත සකසමින්..."

#. Type: error
#. Description
#. Same as base-installer/kernel/failed-package-install.
#. :sl4:
#: ../templates:8001
msgid "Unable to install ${PACKAGE}"
msgstr "${PACKAGE} ස්ථාපනය කල නොහැකි විය"

#. Type: error
#. Description
#. Same as base-installer/kernel/failed-package-install.
#. :sl4:
#: ../templates:8001
msgid ""
"An error was returned while trying to install the ${PACKAGE} package onto "
"the target system."
msgstr "${PACKAGE} පැකේජය ඉලක්කගත පද්ධතියට ස්ථාපනය කිරීමට තැත් කිරීමේදී දෝශයක් ලැබිනි."

#. Type: error
#. Description
#. Same as base-installer/kernel/failed-package-install.
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../templates:8001 ../templates:9001 ../templates:10001 ../templates:11001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "තොරතුරු සඳහා /var/log/syslog හෝ 4 වැනි අතත්‍ය කොන්සෝලය පිරික්සන්න."

#. Type: error
#. Description
#. :sl4:
#: ../templates:9001
#, fuzzy
msgid "No usable ChromeOS kernel partition is found"
msgstr "root කොටසක් හමු නොවිනි"

#. Type: error
#. Description
#. :sl4:
#: ../templates:9001
#, fuzzy
msgid "An error was returned while searching for a ChromeOS kernel partition."
msgstr "ඉලක්කගත පද්ධතිය තුළට කර්නලය ස්ථාපනය කිරීමට තැත්කිරීමේදී දෝශයක් ඇතිවිය."

#. Type: error
#. Description
#. :sl4:
#: ../templates:10001
msgid "Cannot build a boot image"
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../templates:10001
#, fuzzy
msgid "An error was returned while building a boot image."
msgstr "ඉලක්කගත පද්ධතිය තුළට කර්නලය ස්ථාපනය කිරීමට තැත්කිරීමේදී දෝශයක් ඇතිවිය."

#. Type: error
#. Description
#. :sl4:
#: ../templates:11001
#, fuzzy
msgid "Cannot write boot image to disk"
msgstr "ආරම්භක පිළිබිඹුව තැටිය මත සකසමින්..."

#. Type: error
#. Description
#. :sl4:
#: ../templates:11001
#, fuzzy
msgid "An error was returned while writing the boot image to disk."
msgstr "තැටි වෙත වෙනත්කම් ලිවීමේ දෝශයක් හට ගැණිනි."

#. Type: boolean
#. Description
#. :sl4:
#: ../templates:12001
msgid "Reconfigure initramfs policies?"
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../templates:12001
msgid ""
"Could not generate a small enough boot image for this board. Usually this "
"can be resolved by including less modules in the initramfs."
msgstr ""
